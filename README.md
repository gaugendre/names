# Names
This is a collection of names from US and French Governments Open Data.

## Sources
- [US](https://catalog.data.gov/dataset/baby-names-from-social-security-card-applications-national-data)
- [FR](https://www.data.gouv.fr/fr/datasets/liste-des-prenoms-par-annee-prs/)
